<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class ValeurTest extends TestCase
{

    /**
     * @dataProvider additionProvider
     */
    public function testAdd($a, $b, $c, $expected)
    {
        $val = new \App\Models\Valeur;
        $this->assertEquals($val->calcul($a,$b,$c),$expected);
    }

    public function additionProvider()
    {
        return [
            [2, 2, 4, 12.0],
            [1, 0, 0, 0],
            [5, 1, -1, 0],
            [2, -2, 1, -2],
            [1, 4.5, 2.3, 6.8]
        ];
    }

    /**
     * Test de la fonction calcul
     *
     * @return void
     */
    /*public function testCalcul()
    {
        $val = new \App\Models\Valeur;
        $this->assertEquals($val->calcul(2,2,4),12.0);
        $this->assertEquals($val->calcul(1,0,0),0);
        $this->assertEquals($val->calcul(5,1,-1),0);
        $this->assertEquals($val->calcul(2,-2,1),-2);
        $this->assertEquals($val->calcul(1,4.5,2.3),6.8);
    }*/


}
