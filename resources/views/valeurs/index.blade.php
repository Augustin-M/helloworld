@extends('layouts.app')

@section('content')
<h1>&nbsp;Calcul</h1>

@if ($errors->any())
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

<form method="POST" action="/valeurs">
@csrf
    <!--Bouton n°1-->
    &nbsp;<label for="valeur1"><p>&nbsp;Votre calcul:</p></label>
    <select name="valeur1" id="valeur1">
        <option value="">--Choisissez une valeur--</option>
        @foreach ($valeurs as $valeur)
        <option value={{$valeur->valeur}} {{isset($a) ? (($valeur->valeur == $a) ? 'selected' : '') : ''}}>{{$valeur->valeur}}</option>              
        @endforeach
    </select>

    <!--Bouton n°2-->
    <label for="valeur2"> x (</label>
    <input type="text" id="valeur2" name="valeur2" value="{{ $b ?? 0}}">

    <!--Bouton n°3-->
    <label for="valeur3"> + </label>
    <input type="text" id="valeur3" name="valeur3" value="{{ $c ?? 0}}">
    )
    <br><br>
    <!--Bouton n°4-->
    &nbsp;<button type="submit">Valider</button>
</form>

    @if (isset($result))
        <br><p>&nbsp;Résultat: {{ $result }}</p>
    @endif
    
    <br><br>
    <p>Figure</p>



        @foreach ($figures as $figure)
        <?php $poly=$figure->geojson;
        $poly=validePoint($poly);
        isset($b) ? $poly= "$b $c" . substr($poly, 2): '';
        ?>
        <svg height="500" width="500">
            <polygon points="<?php echo $poly; ?>" style="fill:lime;stroke:purple;stroke-width:1" />
            Votre navigateur ne supporte pas le svg.
        </svg>
        @endforeach
    <br><br>

    <?php
function validePoint($arg)
{
     $prefix = 'POLYGON((';
    if (substr($arg, 0, strlen($prefix)) == $prefix) {
        $arg = substr($arg, strlen($prefix));
    } 
    $arg = rtrim($arg, ")) ");

    return $arg;
}
?>


</body>
</html> 
@endsection