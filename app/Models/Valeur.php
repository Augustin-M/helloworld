<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Valeur extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['valeur'];

    /**
     * calcul 3 entiers
     *
     * @param  float $a
     * @param  float $b
     * @param  float $c
     * @return float retourne ($a * ($b + $c))
     */
    public function calcul(float $a, float $b, float $c): float
    {
        return ($a * ($b + $c));
    }
}