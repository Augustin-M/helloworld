<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Valeur;
use App\Models\Figure;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;

class ValeurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
        $parcelles=DB::select('select ST_AsText(figure) as geojson from geo');

        return view('valeurs.index', [
            'valeurs' => Valeur::all(),
            'figures' => $parcelles,
        ]);
    }

    /**
     * Envoie la requete au model pour faire le calcul puis retourne la vue
     *
     * @param Valeur $val
     * @return View
     * @throws ValidationException
     */
    public function calculer(Valeur $val): View
    {
        $this->validate(request(), [
            'valeur1' => ['required'],           
            'valeur2' => ['required', 'integer'],
            'valeur3' => ['required', 'integer'],

        ]);

        $a = request()->input('valeur1');
        $b = request()->input('valeur2');
        $c = request()->input('valeur3');
        $parcelles=DB::select('select ST_AsText(figure) as geojson from geo');
        $result = $val->calcul($a,$b,$c);

        return view('valeurs.index', [
            'valeurs' => Valeur::all(),
            'result' => $result,
            'figures' => $parcelles,
            'a' => $a,
            'b' => $b,
            'c' => $c,
        ]);
    }

        

}
