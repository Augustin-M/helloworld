<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Valeur;
class ValeursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        DB::table('valeurs')->truncate();
        DB::table('valeurs')->insert([
            'valeur' => 1,
        ]);
        DB::table('valeurs')->insert([
            'valeur' => 2,
        ]);
        DB::table('valeurs')->insert([
            'valeur' => 5,
        ]);
    }
}
