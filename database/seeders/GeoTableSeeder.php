<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Figure;
class GeoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('geo')->truncate();
        DB::table('geo')->insert([
            'figure' => 'POLYGON((0 0, 100 0, 100 100, 0 100, 0 0))',
        ]);
        DB::table('geo')->insert([
            'figure' => 'POLYGON((0 0, 100 0, 100 100, 0 0))',
        ]);
        DB::table('geo')->insert([
            'figure' => 'POLYGON((0 0, 50 0, 100 100, 0 100, 0 0))',
        ]);
    }
}
